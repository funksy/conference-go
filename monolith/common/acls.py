import json
import requests
from common.keys import PEXELS_API_KEY as PEX_KEY, OPEN_WEATHER_API_KEY as OW_KEY
# from common.keys import


def get_image_url(city, state):
    headers = {'Authorization': PEX_KEY}
    params = {
        'query': [city, state],
        'locale': 'en-US',
    }
    response = requests.get('https://api.pexels.com/v1/search', params=params, headers=headers)
    content = json.loads(response.content)
    img_url = content["photos"][0]["url"]
    return img_url


def get_lat_lon_by_location(city, state):
    params = {
        'appid': OW_KEY,
        'q' : f'{city}, {state}',
        'limit': 1,
    }
    response = requests.get('http://api.openweathermap.org/geo/1.0/direct', params=params)
    content = json.loads(response.content)
    if not content:
        return None, None
    lat = content[0]["lat"]
    lon = content[0]["lon"]
    return lat, lon


def get_weather_by_location(city, state):
    lat, lon = get_lat_lon_by_location(city, state)
    if lat == None and lat == None:
        return None
    params = {
        'lat': lat,
        'lon': lon,
        'appid': OW_KEY,
        'units': 'imperial',
    }
    response = requests.get('https://api.openweathermap.org/data/2.5/weather', params=params)
    if response.status_code == 400:
        return None
    content = json.loads(response.content)
    weather = {}
    weather['description'] = content['weather'][0]['description']
    weather['temp'] = content['main']['temp']
    return weather
